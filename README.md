The project is written in Java using the JavaFX library for UI elements.

The project only currently has a function registration system.

A user can create a game and are shown a Tic Tac Toe game but all they can do is put X's on the game board.

The registration page features:
* Username Input
* Password Input
* Password Confirm
* Error message for no username/password entered
* Error message for existing username (Compares to database)
* Error message for password different to confirmation
* Password hashing and salting using BCrypt
* Link to login page


The login page features:
* Username Input
* Password Input
* Error message for no username/password entered
* Error on incorrect username/password (Using BCyypt)

Below is some screenshots of the software:

![alt text](https://gitlab.com/BIubofnkas/TicTacToe/raw/master/pictures/1.png)

![alt text](https://gitlab.com/BIubofnkas/TicTacToe/raw/master/pictures/2.png)

![alt text](https://gitlab.com/BIubofnkas/TicTacToe/raw/master/pictures/3.png)

![alt text](https://gitlab.com/BIubofnkas/TicTacToe/raw/master/pictures/4.png)

![alt text](https://gitlab.com/BIubofnkas/TicTacToe/raw/master/pictures/5.png)

![alt text](https://gitlab.com/BIubofnkas/TicTacToe/raw/master/pictures/6.png)

![alt text](https://gitlab.com/BIubofnkas/TicTacToe/raw/master/pictures/7.png)

![alt text](https://gitlab.com/BIubofnkas/TicTacToe/raw/master/pictures/8.png)
