package im;/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author unknown
 */
public class ChatMessage implements Serializable {

    protected static final long serialVersionUID = 1L;
    public static final int REGISTER = 1, DEREGISTER = 2, MESSAGE = 3, FILE = 4, EMESSAGE = 5, EFILE = 6, USERLIST = 7, KEY = 8;
    private int type;
    private String message;
    private String encryptType;
    private String from;
    private String to;
    private ArrayList<String> users;
    private byte[] byteArray;

    public ChatMessage(int type, String message) {
        this.type = type;
        this.message = message;
        System.out.println(message);
    }

    public ChatMessage(int type, String message, byte[] byteArray) {
        this.type = type;
        this.message = message;
        this.byteArray = byteArray;
    }

    public ChatMessage(int type, ArrayList<String> users) {
        this.type = type;
        this.users = users;
    }

    public ChatMessage(int type, String message, String to, String from) {
        this.type = type;
        this.message = message;
        this.to = to;
        this.from = from;
    }

    public ChatMessage(int type, String message,String encryptType, String to, String from) {
        this.type = type;
        this.message = message;
        this.encryptType = encryptType;
        this.to = to;
        this.from = from;
    }

    public ChatMessage(int type, String message, byte[] byteArray, String to, String from) {
        this.type = type;
        this.message = message;
        this.byteArray = byteArray;
        this.to = to;
        this.from = from;
    }

    public ChatMessage(int type, String message, String encryptType, byte[] byteArray, String to, String from) {
        this.type = type;
        this.message = message;
        this.encryptType = encryptType;
        this.byteArray = byteArray;
        this.to = to;
        this.from = from;
    }

    // getters
    public int getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public byte[] getByteArray() {
        return byteArray;
    }

    public String getEncryptType() {
        return encryptType;
    }

    public void setEncryptType(String encryptType) {
        this.encryptType = encryptType;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public ArrayList<String> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<String> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "ChatMessage{" + "type=" + type + ", message=" + message + ", encryptType=" + encryptType + ", from=" + from + ", to=" + to + ", users=" + users + ", byteArray=" + Arrays.toString(byteArray)+ '}';
    }
}
