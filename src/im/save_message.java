package im;

import java.sql.*;


public class save_message {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    public void readDataBase(String to, String from, String message) throws Exception {
        try {




            Class.forName("org.mariadb.jdbc.Driver");

            connect = DriverManager
                    .getConnection("jdbc:mariadb://localhost/feedback?"
                            + "user=sqluser&password=sqluserpw");

            // Statements allow to issue SQL queries to the database
            statement = connect.createStatement();
            // Result set get the result of the SQL query
            resultSet = statement
                    .executeQuery("USE feedback");

            preparedStatement = connect
                    .prepareStatement("INSERT INTO messages VALUES(default, ?, ?, ?, default)");

            preparedStatement.setString(1, to);
            preparedStatement.setString(2, from);
            preparedStatement.setString(3, message);

            resultSet = preparedStatement.executeQuery();


        } catch (Exception e) {
            throw e;
        } finally {
            close();
        }
    }




    // You need to close the resultSet
    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }



}