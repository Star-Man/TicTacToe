package im;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chethan
 */
public interface MessageGUI {

    void writeToChatBox(ChatMessage message);

    void addUser(String user);

    void removeUser(String user);

    String getFileLocation(String from);

   
}
