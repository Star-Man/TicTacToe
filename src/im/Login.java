package im;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class Login {

    private static String saved_address;
    private static String saved_port;

    @FXML
    private TextField username_field;

    @FXML
    private PasswordField password_field;

    @FXML
    private TextField address_field;

    @FXML
    private TextField port_field;

    @FXML
    private Button login_button;

    @FXML
    private Button register_button;

    public Login() {
    }

    private static String logged_in_username;


    @FXML
    protected void register(ActionEvent event) throws IOException {
        System.out.println("Clicked register button");


        Parent register_parent = FXMLLoader.load(getClass().getClassLoader().getResource("im/register.fxml"));
        Scene register_scene = new Scene(register_parent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(register_scene);
        window.setTitle("Tic Tac Toe Registration");

        window.show();
    }


    @FXML
    protected void login(ActionEvent event) throws Exception {
        String username = username_field.getText();
        String password = password_field.getText();
        String address = address_field.getText();
        String port = port_field.getText();


        if(username.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta have a name");

            alert.showAndWait();
        }


        else if(password.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Gotta enter a password");

            alert.showAndWait();
        }


        else {
            System.out.println("All data entered");

            login_mysql dao = new login_mysql();
            if (dao.readDataBase(username, password) == true) {
                logged_in_username = username;

                if (address.isEmpty()){
                    saved_address = "127.0.0.1";
                }
                else{
                    saved_address = address;

                }

                if (port.isEmpty()){
                    saved_port = "9001";
                }
                else{
                    saved_port = port;

                }



                Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("im/message_screen.fxml"));

                Scene register_scene = new Scene(root);
                Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
                window.setScene(register_scene);
                window.setTitle("Terminator 3");

                window.show();
            }




        }



    }

    public static String logged_in_username()
    {
        return logged_in_username;
    }
    public static String saved_address()
    {
        return saved_address;
    }
    public static String saved_port()
    {
        return saved_port;
    }


}