package im;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class get_user_list {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private ResultSet resultSet2 = null;

    public List<String> readDataBase() throws Exception {
        List<String> usernames;
        try {




            Class.forName("org.mariadb.jdbc.Driver");

            connect = DriverManager
                    .getConnection("jdbc:mariadb://localhost/feedback?"
                            + "user=sqluser&password=sqluserpw");

            // Statements allow to issue SQL queries to the database
            statement = connect.createStatement();
            // Result set get the result of the SQL query
            resultSet = statement
                    .executeQuery("USE feedback");

            preparedStatement = connect
                    .prepareStatement("select * from users");



            resultSet = preparedStatement.executeQuery();

            usernames = view_users(resultSet);

        } catch (Exception e) {
            throw e;
        } finally {
            close();
        }
        return usernames;
    }



    private List<String> view_users(ResultSet resultSet) throws SQLException {
        // ResultSet is initially before the first data set
        List<String> usernames = new ArrayList<String>();
        while (resultSet.next()) {

            usernames.add(resultSet.getString("username"));

        }
        return usernames;

    }

    // You need to close the resultSet
    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }



}