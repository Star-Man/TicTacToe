package im;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.swing.*;
import java.io.*;
import java.security.Key;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


public class message_screen extends Application implements MessageGUI {


    private javax.swing.JList userList;
    private String logged_in_user = "";
    private String address;
    private String port;
    public static client Client;
    private String selected_user;
    private String encrypt_type = "none";
    List<String> usernames;
    List<Button> username_list = new LinkedList<Button>();
    HashMap<String, Message> messageWindows = new HashMap<String, Message>();
    static DefaultListModel<String> userListModel = new DefaultListModel();




    @FXML
    private VBox maingrid;


    @FXML
    private MenuButton encrypt_button;

    @FXML
    private Button upload_button;

    @FXML
    private TextField message_input_box;

    @FXML
    private TextFlow message_box;

    @FXML private ScrollPane message_scroll;

    @FXML
    protected void initialize() throws Exception {
        logged_in_user = Login.logged_in_username();
        address = Login.saved_address();
        port = Login.saved_port();


        get_user_list dao = new get_user_list();
        usernames = dao.readDataBase();
        System.out.println("Logged in user is: " + logged_in_user);

        for (int i = 0; i < usernames.size(); i++) {
                username_list.add(new Button());

                username_list.get(i).setText(usernames.get(i));
                int finalI = i;

                username_list.get(i).setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        select_user(finalI);

                    }

                });


                username_list.get(i).prefWidthProperty().bind(maingrid.widthProperty());
                username_list.get(i).getStylesheets().add("im/stylesheet2.css");
            if (!usernames.get(i).equals(logged_in_user)){

                maingrid.getChildren().add(username_list.get(i));
            }


        }


        Client = new client(address, port, logged_in_user, this);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Client.listenFromServer();
            }
        }).start();
    }



    @FXML
    protected void rsa_clicked(){
        encrypt_type = "rsa";
        message_input_box.setPromptText("Send an RSA encrypted message...");
    }

    @FXML
    protected void aes_clicked() throws Exception {
        encrypt_type = "aes";
        message_input_box.setPromptText("Send an AES encrypted message...");
        Key aes_key = Security.getAESKey();
        System.out.println("AES KEY: " + aes_key);
    }

    @FXML
    protected void no_encrypt_clicked(){
        encrypt_type = "none";
        message_input_box.setPromptText("Send an unencrypted message...");
    }

    @FXML
    public void onEnter(ActionEvent ae) throws Exception {

        String text = message_input_box.getText();
        Message clientMessage;
        if (text != null && !text.isEmpty()) {
            System.out.println("Text entered");
            Security.AES(text);
            clientMessage = new Message(selected_user);
            messageWindows.put(selected_user, clientMessage);


            clientMessage.chatMessages.append(Client.name).append("\n").append(text);
            clientMessage.chatMessages.append("\n");
            appendToChatBox(logged_in_user, text);
            if (encrypt_type.contains("none")) {
                Client.sendReqToServer(new ChatMessage(ChatMessage.MESSAGE, text, selected_user, Client.getName()));

            }
            message_input_box.setText("");

        }
        message_scroll.layout();
        message_scroll.setVvalue(1.5);

    }


    public void fileBtnActionPerformed(ActionEvent actionEvent) {
        String to = selected_user;
        Stage window = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();

        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(window);


        System.out.println("Reading and sending file");
        System.out.println("file name : " + file.getName());
        //fileLabel.setText(file.getName());
        FileInputStream fin;
        ChatMessage message;
        try {
            byte[] bytearray = new byte[(int) file.length()];
            fin = new FileInputStream(file);
            BufferedInputStream bin = new BufferedInputStream(fin);
            bin.read(bytearray, 0, bytearray.length);
            if (encrypt_type.contains("none")) {
                message = new ChatMessage(ChatMessage.FILE, file.getName(), bytearray, to, Client.getName());
                Client.sendReqToServer(message);
            } /*else {
                Message clientMessage;
                //CipherMode cipherMode;
                clientMessage = messageWindows.get(to);
                if (clientMessage == null) {
                    clientMessage = new Message(to);
                    messageWindows.put(to, clientMessage);

                }

                switch (encryptType) {
                    case "RSA":
                        if (clientMessage.getRsa() == null) {
                            RSA rsa = new RSA();
                            clientMessage.setCipherMode(rsa);
                            client.sendReqToServer(new ChatMessage(ChatMessage.KEY, "RSA", rsa.getPublicKey().getEncoded(), to, client.getName()));
                            System.out.println("Sent key to client : " + to);
                        }

                        break;
                    case "AES":
                        if (clientMessage.getAes() == null) {
                            AES aes = new AES();
                            clientMessage.setCipherMode(aes);
                            client.sendReqToServer(new ChatMessage(ChatMessage.KEY, "AES", aes.getSecretKey().getEncoded(), to, client.getName()));
                            System.out.println("Sent key to client : " + to);
                        }

                        break;
                }
                cipherMode = encryptType.equals("RSA") ? clientMessage.getRsa() : clientMessage.getAes();
                bytearray = cipherMode.encrypt(bytearray);*//*
            // message = new ChatMessage(ChatMessage.EFILE, file.getName(), encryptType, bytearray, to, client.getName());
            // }

            //client.sendReqToServer(message);
            */
        } catch (FileNotFoundException ex) {
            System.err.println("FIleNotFound Exception: " + ex);
        } catch (IOException ex) {
            System.err.println("IO Exception: " + ex);
        }




    }



    public void appendToChatBox(String user, String message) {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {


                if( user.equals(selected_user) || user.equals(logged_in_user) ){
                    Text user_text = new Text(user + "\n");
                    Text message_text = new Text(message + "\n\n");

                    user_text.setStyle("-fx-font: 12 Helvetica; -fx-fill: #616161;");
                    message_text.setStyle("-fx-font: 14 Helvetica; -fx-fill: #dadada;");


                    message_box.getChildren().add(user_text);
                    message_box.getChildren().add(message_text);

                }
                else {

                    selected_user = user;
                    int finalI = 0;
                    for (int i = 0; i < usernames.size(); i++) {

                        String text = username_list.get(i).getText();
                        if (user.equals(text)){
                            finalI = i;
                            break;
                        }

                    }
                    select_user(finalI);
                }
            }
        });

    }


    @Override
    public void writeToChatBox(ChatMessage message) {
        System.out.println("Received " + message.getMessage() + " from " + message.getFrom());
        Message clientMessage;
        switch (message.getType()) {
            case ChatMessage.MESSAGE:
                System.out.println("Message has MESSAGE type");

                appendToChatBox(message.getFrom(), message.getMessage());
                break;

            case ChatMessage.EFILE:
            case ChatMessage.FILE:
                System.out.println("Message has FILE type");

                userList.setSelectedValue(message.getFrom(), true);
                clientMessage = messageWindows.get(message.getFrom());
                if (clientMessage == null) {
                    clientMessage = new Message(message.getFrom());
                    messageWindows.put(message.getFrom(), clientMessage);
                }
                clientMessage.chatMessages.append(message.getFrom()).append(" : sent file : ").append(message.getMessage()).append("\n");
                appendToChatBox(message.getFrom(), message.getMessage());
                break;


        }
    }



    @Override
    public  void addUser(String username) {
        if (!userListModel.contains(username)) {

            userListModel.addElement(username);

            for (int i = 0; i < usernames.size(); i++) {

                String x = username_list.get(i).getText();
                if (x.equals(username)){
                    username_list.get(i).setStyle("-fx-text-fill: #E5E5E5");

                }

            }

        }
    }

    @Override
    public  void removeUser(String username) {
        if (userListModel.contains(username)) {
            userListModel.removeElement(username);
        }
    }

    @Override
    public String getFileLocation(String username) {
        return "C:\\";
    }





    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("im/login.fxml"));

        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();


    }


    public static void main(String[] args) {
        launch(args);

    }

    public void select_user(int finalI) {

        selected_user = usernames.get(finalI);
        for (int x = 0; x < (usernames.size()); x++) {
            username_list.get(x).setStyle("-fx-background-color: none;");
            String z = username_list.get(x).getText();

            if (userListModel.contains(z)) {
                username_list.get(x).setStyle("-fx-text-fill: #E5E5E5");
            }


        }
        username_list.get(finalI).setStyle("-fx-background-color: rgba(235, 164, 33, 1);");

        message_input_box.setVisible(true);
        upload_button.setVisible(true);
        encrypt_button.setVisible(true);
        message_box.getChildren().clear();

        get_messages dao1 = new get_messages();
        List<String> messages = null;
        try {
            messages = dao1.readDataBase(logged_in_user, selected_user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String user = null;
        for (int x = 0; x < (messages.size()); x++) {

            if (!((x & 1) == 0)) {
                String message = messages.get(x);
                Text user_text = new Text(user + "\n");
                Text message_text = new Text(message + "\n\n");

                user_text.setStyle("-fx-font: 12 Helvetica; -fx-fill: #616161;");
                message_text.setStyle("-fx-font: 14 Helvetica; -fx-fill: #dadada;");


                message_box.getChildren().add(user_text);
                message_box.getChildren().add(message_text);

            } else {
                user = messages.get(x);

            }


        }

        message_scroll.layout();
        message_scroll.setVvalue(1.5);


    }
    
}




