package server;

import im.ChatMessage;
import im.save_message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chethan
 */
public class Server {

    private int port;
    private ServerSocket listener;
    private static ServerGUI sgui;
    private static HashMap<String, Handler> clients = new HashMap<String, Handler>();
    private boolean listen = true;

    public Server(int port, ServerGUI sgui) {
        try {
            System.out.println("Starting server");
            System.out.println("port : " + port);
            this.port = port;
            Server.sgui = sgui;
            this.listener = new ServerSocket(port);
            System.out.println("Server Started");
            sgui.appendEvent("Server Started");
        } catch (IOException ex) {
            System.err.println("Failed to start server : " + ex);
        }
    }

    public void startListener() {
        try {
            Handler client;
            System.out.println("Started Listening");
            sgui.appendEvent("Started Listening");
            while (listen) {
                Socket socket = listener.accept();
                client = new Handler(socket);
                client.start();
            }
        } catch (IOException ex) {
            System.err.println("Error starting listener" + ex);
        } catch (Exception ex) {
            System.err.println("Error starting listener" + ex);
        } finally {
            try {
                listener.close();
            } catch (IOException ex) {
                System.err.println("Error Closing listener" + ex);

            }
        }
    }

    private static synchronized void broadcast(ChatMessage message) {
        for (String clientName : clients.keySet()) {
            Handler client = clients.get(clientName);
            client.writeMessage(message);
            System.out.println("Sending client : " + clientName + " " + message);
        }
    }

    private static synchronized void sendMessageToClient(ChatMessage message) {
        System.out.println("Sending " + message.getMessage() + " to " + message.getTo());
        clients.get(message.getTo()).writeMessage(message);
    }

    private static synchronized void deregisterClient(String name) {
        clients.remove(name);
    }

    private static synchronized ChatMessage getUserList() {
        return new ChatMessage(ChatMessage.USERLIST, new ArrayList<>(clients.keySet()));
    }

    public void stop() {
        clients = null;
        listen = false;
        try {
            listener.close();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    static class Handler extends Thread {

        private Socket socket;
        private ObjectInputStream in;
        private ObjectOutputStream out;
        private ChatMessage message;
        private String username;

        /**
         * Constructs a handler thread, squirreling away the socket. All the
         * interesting work is done in the run method.
         */
        public Handler(Socket socket) throws IOException {
            this.socket = socket;
            this.out = new ObjectOutputStream(socket.getOutputStream());
            this.in = new ObjectInputStream(socket.getInputStream());

        }

        /**
         * Services this thread's client by repeatedly requesting a screen name
         * until a unique one has been submitted, then acknowledges the name and
         * registers the output stream for the client in a global set, then
         * repeatedly gets inputs and broadcasts them.
         */
        @Override
        public void run() {
            try {
                boolean keepGoing = true;
                while (keepGoing) {
                    message = (ChatMessage) in.readObject();
                    System.out.println("Message : " + message);
                    switch (message.getType()) {
                        case ChatMessage.REGISTER:
                            this.username = message.getMessage();
                            System.out.println("user registered : " + username);
                            clients.put(username, this);
                            broadcast(getUserList());
                            sgui.addUser(username);
                            sgui.appendEvent(username + " registered");
                            break;

                        case ChatMessage.DEREGISTER:
                            username = message.getMessage();
                            System.out.println("user deregistered : " + username);
                            deregisterClient(username);
                            sgui.removeUser(username);
                            broadcast(message);
                            sgui.appendEvent(username + " deregistered");
                            break;

                        case ChatMessage.MESSAGE:
                            sgui.appendEvent(username + " sent message to " + message.getTo());
                            try {

                                save_message dao = new save_message();
                                try {
                                    dao.readDataBase(message.getTo() ,message.getFrom(), message.getMessage());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                                sendMessageToClient(message);
                            } catch (Exception ex) {
                                System.err.println("Error with MESSAGE type command");

                                System.err.println(ex);
                            }
                            break;
                        case ChatMessage.EMESSAGE:
                            sgui.appendEvent(username + " sent message to " + message.getTo() + " encryption : " + message.getMessage());
                            try {
                                sendMessageToClient(message);
                            } catch (Exception ex) {
                                System.err.println("Error with MESSAGE type command");

                                System.err.println(ex);
                            }
                            break;
                        case ChatMessage.FILE:
                            sgui.appendEvent(username + " sent file to " + message.getTo());
                            try {
                                sendMessageToClient(message);
                            } catch (Exception ex) {
                                System.err.println("Error with FILE type command");

                                System.err.println(ex);
                            }
                            break;
                        case ChatMessage.EFILE:
                            sgui.appendEvent(username + " sent file to " + message.getTo() + " encryption : " + message.getEncryptType());
                            try {
                                sendMessageToClient(message);
                            } catch (Exception ex) {
                                System.err.println("Error with EFILE type command");

                                System.err.println(ex);
                            }
                            break;
                        case ChatMessage.KEY:
                            sgui.appendEvent(username + " sent key to " + message.getTo());
                            try {
                                sendMessageToClient(message);
                            } catch (Exception ex) {
                                System.err.println("Error with KEY type command");

                                System.err.println(ex);
                            }
                            break;


                        default:
                            sgui.appendEvent(username + ": " + message.getMessage() + "\n");
                            message.setMessage(username + ": " + message.getMessage() + "\n");
                            broadcast(message);
                            break;
                    }
                }
            } catch (ClassNotFoundException | IOException ex) {
                System.err.println(ex);
                System.err.println("Super error");
            }
        }

        public boolean writeMessage(ChatMessage message) {
            System.out.println("Writing message");

            boolean flag = false;
            if (!socket.isConnected()) {
                close();
                flag = false;
            } else {
                try {
                    out.writeObject(message);
                    flag = true;
                } // if an error occurs, do not abort just inform the user
                catch (IOException e) {
                    System.err.println("Error sending message to " + username);
                }
            }
            return flag;
        }

        private void close() {
            // try to close the connection
            try {
                if (out != null) {
                    out.close();
                }
            } catch (Exception e) {
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e) {
            }
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (Exception e) {
            }
        }
    }
}
